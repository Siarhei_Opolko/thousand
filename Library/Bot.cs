﻿using System;
using System.Linq;

namespace Library
{
    [Serializable]
    public class Bot : Player , IThousangCs
    {
        private Random random = new Random();
        public PartyState partyState = new PartyState();
        public Card GamerOnTableCard { get; set; }
        public Card GiveCard()
        {
            if (partyState.PrioritySuit == SuitCard.None)
            {
                if (partyState.OpponentCards.Exists(c => c.Value == ValueCard.Ace))
                    return partyState.OpponentCards.Find(c => c.Value == ValueCard.Ace);
                if (partyState.OpponentCards.Exists(c => c.Value == ValueCard.Ten))
                    return partyState.OpponentCards.Find(c => c.Value == ValueCard.Ten);
                if (partyState.OpponentCards.Exists(c => c.GetHashCode() == 43) && partyState.OpponentCards.Exists(d => d.GetHashCode() == 44))
                    return partyState.OpponentCards.Find(c => c.GetHashCode() == 43);
                if (partyState.OpponentCards.Exists(c => c.GetHashCode() == 33) && partyState.OpponentCards.Exists(d => d.GetHashCode() == 34))
                    return partyState.OpponentCards.Find(c => c.GetHashCode() == 33);
                if (partyState.OpponentCards.Exists(c => c.GetHashCode() == 23) && partyState.OpponentCards.Exists(d => d.GetHashCode() == 24))
                    return partyState.OpponentCards.Find(c => c.GetHashCode() == 23);
                if (partyState.OpponentCards.Exists(c => c.GetHashCode() == 13) && partyState.OpponentCards.Exists(d => d.GetHashCode() == 14))
                    return partyState.OpponentCards.Find(c => c.GetHashCode() == 13);
                if (partyState.OpponentCards.Exists(c => c.Value == ValueCard.King))
                    return partyState.OpponentCards.Find(c => c.Value == ValueCard.King);
                if (partyState.OpponentCards.Exists(c => c.Value == ValueCard.Queen))
                    return partyState.OpponentCards.Find(c => c.Value == ValueCard.Queen);
                if (partyState.OpponentCards.Exists(c => c.Value == ValueCard.Knave))
                    return partyState.OpponentCards.Find(c => c.Value == ValueCard.Knave);
                if (partyState.OpponentCards.Exists(c => c.Value == ValueCard.Nine))
                    return partyState.OpponentCards.Find(c => c.Value == ValueCard.Nine);
            }
            else
            {
                if (
                    partyState.OpponentCards.Exists(
                        c => c.Suit == partyState.Table.Values.ToList()[0].Suit && c.GetPointsCard() > partyState.Table.Values.ToList()[0].GetPointsCard()))
                    return
                        partyState.OpponentCards.Find(
                            c => c.Suit == partyState.Table.Values.ToList()[0].Suit && c.GetPointsCard() > partyState.Table.Values.ToList()[0].GetPointsCard());

                if (partyState.OpponentCards.Exists(
                    c => c.Suit == partyState.Table.Values.ToList()[0].Suit && c.GetPointsCard() < partyState.Table.Values.ToList()[0].GetPointsCard()))
                {
                    var q = partyState.OpponentCards.Where(c => c.Suit == partyState.Table.Values.ToList()[0].Suit);

                    ValueCard card;
                    card = q.Min(x => x.Value);
                    return partyState.OpponentCards.First(w => w.Value == card && w.Suit == partyState.Table.Values.ToList()[0].Suit);
                }

                if (!partyState.OpponentCards.Exists(c => c.Suit == partyState.Table.Values.ToList()[0].Suit))
                {
                    var q = partyState.OpponentCards.Min(c => c.GetPointsCard());
                    return partyState.OpponentCards.First(c => c.GetPointsCard() == q);
                }
            }
            return partyState.OpponentCards[random.Next(0, partyState.OpponentCards.Count)];
        }
        public int SetDesiredPoints(int playerBet)
        {
            if(random.NextDouble() < 0.4) return playerBet += 5;
            return playerBet;
        }
        public void SelectTwoCards(out Card a, out Card b)
        {
            var c = partyState.OpponentCards.Where(x => x.Value == ValueCard.Nine || x.Value == ValueCard.Knave || x.Value == ValueCard.Queen).ToList();

            a = c[0];
            b = c[1];//?
        }

        public PartyState GiveAwayCards(Player player, Card firstCard, Card secondCard)
        {
            lock (partyState.OpponentCards)
            {
                partyState.OpponentCards.Remove(firstCard);
                partyState.OpponentCards.Remove(secondCard);
            }

            lock (partyState.DeckGamerList)
            {
                partyState.DeckGamerList.Add(firstCard);
                partyState.DeckGamerList.Add(secondCard);
            }

            partyState.WhoIsOnPrikupNow = null;
            partyState.IsBattle = true;
            partyState.WhoIsHitNow = "Gomer";

            return partyState;
        }

        public PartyState Hit(Player player, Card card)
        {
            if (partyState.IsBattle && card.IsCorrect(partyState.PrioritySuit, partyState.OpponentCards))
            {
                var botBribes = partyState.BotBribes;
                partyState.CheckTrump(ref botBribes, card,partyState.OpponentCards);
                partyState.BotBribes = botBribes;
                lock (partyState.OpponentCards)
                {
                    partyState.OpponentCards.Remove(card);
                    partyState.WhoIsHitNow = null;
                }
                lock (partyState.Table)
                {
                    partyState.Table.Add("Gomer", card);
                }

                if (partyState.Table.Count != 2)
                    partyState.WhoIsHitNow = partyState.PlayerName;

                if (partyState.Table.Count == 1)
                {
                    partyState.PrioritySuit = partyState.Table.Values.ToList()[0].Suit;
                }
            }
            return partyState;
        }

        public PartyState StartParty()
        {
            throw new NotImplementedException();
        }

        public PartyState GetPartyState()
        {
            return partyState;
        }

        public PartyState ExitGame()
        {
            throw new NotImplementedException();
        }

        public PartyState SetBet(Player player, int points)
        {
            if (points <= partyState.BotPotentialPoints && points > partyState.GamerCurrentBet)
            {
                partyState.OpponentCurrentBet = points;
                partyState.WhoIsSetBetNow = partyState.PlayerName;
            }
            return partyState;
        }

        public PartyState NoSetBet(Player player)
        {
            lock (partyState.DeckGamerList)
            {
                partyState.DeckGamerList.AddRange(partyState.Prikup);
            }

            partyState.WhoIsOnPrikupNow = partyState.PlayerName;
            partyState.WhoIsSetBetNow = null;

            partyState.OpponentCurrentBet = 0;
            partyState.WhoIsHitNow = null;
            
            return partyState;
        }
    }
}
