﻿using System.Collections.Generic;
using System;

namespace Library
{
    [Serializable]
    public class Player
    {
        public string PlayerName { get; set; }
        public List<Card> Cards { get; set; }
        public int Bribe { get; set; }
        public bool OnPrikup { get; set; }
        public int PotentialPoints { get; set; }
        public bool CanISetBet { get; set; }
        public bool CanIGo { get; set; }//
        public int CurrentBet { get; set; }
        public int Points { get; set; }
        public Card OutputCard { get; set; }
        public List<Card> OutputCards { get; set; }
        public bool IsOnHundredPoints { get; set; }
        public int Bolts { get; set; }

        public Player()
        {
            Cards = new List<Card>();
            Bribe = 0;
            OnPrikup = false;
            OutputCard = new Card();
            OutputCards = new List<Card>();
            IsOnHundredPoints = false;
            Points = 0;
            Bolts = 0;
        }
        public Player(bool onprikup)
        {
            Cards = new List<Card>();
            Bribe = 0;
            OnPrikup = onprikup;
        }
        public void CalculatePotentialPoints()
        {
            PotentialPoints = 0;

            PotentialPoints = 120;
            if (Cards.Exists(c => c.GetHashCode() == 13) && Cards.Exists(d => d.GetHashCode() == 14))
                PotentialPoints += 40;

            if (Cards.Exists(c => c.GetHashCode() == 23) && Cards.Exists(d => d.GetHashCode() == 24))
                PotentialPoints += 60;

            if (Cards.Exists(c => c.GetHashCode() == 33) && Cards.Exists(d => d.GetHashCode() == 34))
                PotentialPoints += 80;

            if (Cards.Exists(c => c.GetHashCode() == 43) && Cards.Exists(d => d.GetHashCode() == 44))
                PotentialPoints += 100;
        }
        public void IsPlayedBets()
        {
            if (CurrentBet == 0)
                Points += Bribe;
            else
            {
                if (Bribe >= CurrentBet)
                    Points += CurrentBet;
                else
                {
                    Points -= CurrentBet;
                }
            }
            Points -= Points % 5;
        }

        public void Is555Points()
        {
            if (Points == 555)
                Points = 0;
        }

        public bool IsNoBribe()
        {
            return Bribe == 0;
        }

        public void Reset()
        {
            OnPrikup = false;
            CanIGo = false;
            Cards.Clear();
            Bribe = 0;
            CurrentBet = 0;
            OutputCards.Clear();
        }
    }
}
