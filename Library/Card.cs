﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Library
{
    [Serializable]
    public enum SuitCard
    {
        None = 0,
        Spades = 1,
        Clubs = 2,
        Diamonds = 3,
        Hearts = 4,
    }
    [Serializable]
    public enum ValueCard
    {
        Ace = 6,
        Ten = 5,
        King = 4,
        Queen = 3,
        Knave = 2,
        Nine = 1,
        None = 0
    }
    [Serializable]
    public class Card
    {
        public Card() { }
        public Card(int suit, int value)
        {
            this.Suit = (SuitCard)suit;
            this.Value = (ValueCard)value;
        }
        public SuitCard Suit { get; set; }
        public ValueCard Value { get; set; }
        public int GetPointsCard()
        {
            switch (Value)
            {
                case ValueCard.Ace:
                    return 11;
                case ValueCard.King:
                    return 4;
                case ValueCard.Queen:
                    return 3;
                case ValueCard.Knave:
                    return 2;
                case ValueCard.Ten:
                    return 10;
                case ValueCard.Nine:
                    return 0;
            }
            return 0;
        }
        public override int GetHashCode()
        {
            return (int)Suit * 10 + (int)Value;
        }
        public void NoCard()
        {
            Suit = SuitCard.None;
            Value = ValueCard.None;
        }
        public Bitmap GetBitmap()
        {
            return Components.Images[new KeyValuePair<SuitCard, ValueCard>(Suit, Value)].Clone() as Bitmap;
        }
        public static Bitmap GetFlipside()
        {
            return Components.Flipside.Clone() as Bitmap;
        }
        public override string ToString()
        {
            return String.Format("{0} {1}", Suit, Value);
        }
        public bool IsCorrect(SuitCard prioritySuit, List<Card> cards)
        {
            if (prioritySuit == SuitCard.None) return true;
            if (Suit == prioritySuit) return true;
            if (Suit != prioritySuit && cards.All(c => c.Suit != prioritySuit)) return true;
            return false;
        }
    }
}
