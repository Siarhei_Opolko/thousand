﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Library
{
    public static class Tools
    {
        public static void GenerateCard(this List<Card> cards, int callCount)
        {
            for (int i = 0; i < callCount; i++)
            {
                cards.Add(CardFactory.GenerateCard());
            }
        }

        
        public static byte[] Serialize(PartyState ps)
        {
            byte [] buf = new byte[8192];
            
            using (MemoryStream fs = new MemoryStream(buf))
            {
                 BinaryFormatter formatter = new BinaryFormatter();
                 formatter.Serialize(fs, ps);
            }
            return buf;
        }

        public static PartyState Deserialize(byte [] bytes)
        {
            PartyState partyState = new PartyState();
            using (MemoryStream fs = new MemoryStream(bytes))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                partyState = (PartyState)formatter.Deserialize(fs);
            }
            return partyState;
        }
    }
}
