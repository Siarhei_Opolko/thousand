﻿namespace Library
{
    public interface IThousangCs
    {
        PartyState GiveAwayCards(Player player, Card firstCard, Card secondCard);
        PartyState Hit(Player player, Card card);
        PartyState StartParty();
        PartyState GetPartyState();
        PartyState ExitGame();
        PartyState SetBet(Player player, int points);
        PartyState NoSetBet(Player player);
    }
}
