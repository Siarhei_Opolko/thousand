﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Library
{
    public static class CardFactory
    {
        public static List<Card> DistributeCards = new List<Card>();
        private static Random random = new Random();

        public static Card GenerateCard()
        {
            Card c;

            while (true)
            {
                c = new Card();
                int suit = random.Next(1, 5);
                int value = random.Next(1, 7);
                c.Suit = (SuitCard)suit;
                c.Value = (ValueCard)value;
                if (!DistributeCards.Any(cx => cx.Suit == c.Suit && cx.Value == c.Value))
                    break;
            }
            DistributeCards.Add(c);

            return c;
        }
    }
}
