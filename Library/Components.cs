﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Library
{
    public static class Components
    {
        public static readonly Dictionary<KeyValuePair<SuitCard, ValueCard>, Bitmap> Images = new Dictionary<KeyValuePair<SuitCard, ValueCard>, Bitmap>();
        public static Bitmap Flipside = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\default_800x480\flipside.png"));
        public static void InitCards()
        {
            Bitmap spades9 = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\default_800x480\33.png"));
            Images.Add(new KeyValuePair<SuitCard, ValueCard>(SuitCard.Spades, ValueCard.Nine), spades9);
            Bitmap spades10 = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\default_800x480\34.png"));
            Images.Add(new KeyValuePair<SuitCard, ValueCard>(SuitCard.Spades, ValueCard.Ten), spades10);
            Bitmap spadesKnave = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\default_800x480\35.png"));
            Images.Add(new KeyValuePair<SuitCard, ValueCard>(SuitCard.Spades, ValueCard.Knave), spadesKnave);
            Bitmap spadesQueen = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\default_800x480\36.png"));
            Images.Add(new KeyValuePair<SuitCard, ValueCard>(SuitCard.Spades, ValueCard.Queen), spadesQueen);
            Bitmap spadesKing = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\default_800x480\37.png"));
            Images.Add(new KeyValuePair<SuitCard, ValueCard>(SuitCard.Spades, ValueCard.King), spadesKing);
            Bitmap spadesAce = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\default_800x480\38.png"));
            Images.Add(new KeyValuePair<SuitCard, ValueCard>(SuitCard.Spades, ValueCard.Ace), spadesAce);

            Bitmap clubs9 = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\default_800x480\46.png"));
            Images.Add(new KeyValuePair<SuitCard, ValueCard>(SuitCard.Clubs, ValueCard.Nine), clubs9);
            Bitmap clubs10 = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\default_800x480\47.png"));
            Images.Add(new KeyValuePair<SuitCard, ValueCard>(SuitCard.Clubs, ValueCard.Ten), clubs10);
            Bitmap clubsKnave = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\default_800x480\48.png"));
            Images.Add(new KeyValuePair<SuitCard, ValueCard>(SuitCard.Clubs, ValueCard.Knave), clubsKnave);
            Bitmap clubsQueen = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\default_800x480\49.png"));
            Images.Add(new KeyValuePair<SuitCard, ValueCard>(SuitCard.Clubs, ValueCard.Queen), clubsQueen);
            Bitmap clubsKing = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\default_800x480\50.png"));
            Images.Add(new KeyValuePair<SuitCard, ValueCard>(SuitCard.Clubs, ValueCard.King), clubsKing);
            Bitmap clubsAce = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\default_800x480\51.png"));
            Images.Add(new KeyValuePair<SuitCard, ValueCard>(SuitCard.Clubs, ValueCard.Ace), clubsAce);

            Bitmap diamonds9 = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\default_800x480\7.png"));
            Images.Add(new KeyValuePair<SuitCard, ValueCard>(SuitCard.Diamonds, ValueCard.Nine), diamonds9);
            Bitmap diamonds10 = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\default_800x480\8.png"));
            Images.Add(new KeyValuePair<SuitCard, ValueCard>(SuitCard.Diamonds, ValueCard.Ten), diamonds10);
            Bitmap diamondsKnave = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\default_800x480\9.png"));
            Images.Add(new KeyValuePair<SuitCard, ValueCard>(SuitCard.Diamonds, ValueCard.Knave), diamondsKnave);
            Bitmap diamondsQueen = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\default_800x480\10.png"));
            Images.Add(new KeyValuePair<SuitCard, ValueCard>(SuitCard.Diamonds, ValueCard.Queen), diamondsQueen);
            Bitmap diamondsKing = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\default_800x480\11.png"));
            Images.Add(new KeyValuePair<SuitCard, ValueCard>(SuitCard.Diamonds, ValueCard.King), diamondsKing);
            Bitmap diamondsAce = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\default_800x480\12.png"));
            Images.Add(new KeyValuePair<SuitCard, ValueCard>(SuitCard.Diamonds, ValueCard.Ace), diamondsAce);

            Bitmap hearts9 = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\default_800x480\20.png"));
            Images.Add(new KeyValuePair<SuitCard, ValueCard>(SuitCard.Hearts, ValueCard.Nine), hearts9);
            Bitmap hearts10 = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\default_800x480\21.png"));
            Images.Add(new KeyValuePair<SuitCard, ValueCard>(SuitCard.Hearts, ValueCard.Ten), hearts10);
            Bitmap heartsKnave = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\default_800x480\22.png"));
            Images.Add(new KeyValuePair<SuitCard, ValueCard>(SuitCard.Hearts, ValueCard.Knave), heartsKnave);
            Bitmap heartsQueen = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\default_800x480\23.png"));
            Images.Add(new KeyValuePair<SuitCard, ValueCard>(SuitCard.Hearts, ValueCard.Queen), heartsQueen);
            Bitmap heartsKing = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\default_800x480\24.png"));
            Images.Add(new KeyValuePair<SuitCard, ValueCard>(SuitCard.Hearts, ValueCard.King), heartsKing);
            Bitmap heartsAce = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\default_800x480\25.png"));
            Images.Add(new KeyValuePair<SuitCard, ValueCard>(SuitCard.Hearts, ValueCard.Ace), heartsAce);

            
        }

        public static readonly Dictionary<SuitCard, Bitmap> SuitsBitmaps = new Dictionary<SuitCard, Bitmap>();

        public static void InitSuits()
        {
            Bitmap spades = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\suits_images\spades.png"));
            SuitsBitmaps.Add(SuitCard.Spades, spades);
            Bitmap clubs = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\suits_images\clubs.png"));
            SuitsBitmaps.Add(SuitCard.Clubs, clubs);
            Bitmap diamonds = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\suits_images\diamonds.png"));
            SuitsBitmaps.Add(SuitCard.Diamonds, diamonds);
            Bitmap hearts = new Bitmap(Path.Combine(Application.StartupPath, @"..\..\suits_images\hearts.png"));
            SuitsBitmaps.Add(SuitCard.Hearts, hearts);
        }
    }
}
