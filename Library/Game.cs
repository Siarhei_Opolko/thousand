﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;

namespace Library
{
    public class Game : IThousangCs
    {
        public Bot OpponentPlayer { get; set; }
        public Player Gamer { get; set; }
        
        public PartyState partystate { get; set; }
     
        public Player DefaultHundredPoints { get; set; }
        public Game()
        {
            Gamer = new Player();
            OpponentPlayer = new Bot();
            partystate = new PartyState();
            partystate.WhoIsSetBetNow = null;
        }

        public PartyState InitCardsForParty()
        {
            partystate.PlayerName = Gamer.PlayerName;
            partystate.Prikup.Clear();
            partystate.BonusListCards.Clear();
            CardFactory.DistributeCards.Clear();

            Gamer.Reset();
            OpponentPlayer.Reset();

            while (partystate.Prikup.Sum(x => x.GetPointsCard()) < 20)
            {
                partystate.Prikup.Clear();
                CardFactory.DistributeCards.Clear();
                partystate.Prikup.GenerateCard(4);
            }

            Gamer.Cards.GenerateCard(8);
            OpponentPlayer.Cards.GenerateCard(8);
            partystate.BonusListCards.GenerateCard(4);

            Gamer.CalculatePotentialPoints();
            OpponentPlayer.CalculatePotentialPoints();

            partystate.DeckGamerList = Gamer.Cards;
            partystate.OpponentCards = OpponentPlayer.Cards;
            partystate.TrumpSuit = SuitCard.None;

            return StartParty();
        }

        private Player AnotherPlayer(Player p)
        {
            if (p == Gamer)
                return OpponentPlayer;
            return Gamer;
        }

        private string AnotherName(Player p)
        {
            if (p == Gamer) return OpponentPlayer.PlayerName;
            return Gamer.PlayerName;
        }

        public Player GetPlayerByName(string s)
        {
            if (s == Gamer.PlayerName)
                return Gamer;
            return OpponentPlayer;
        }

        public PartyState GiveAwayCards(Player player, Card firstCard, Card secondCard)
        {
            lock (partystate.DeckGamerList)
            {
                partystate.DeckGamerList.Remove(firstCard);
                partystate.DeckGamerList.Remove(secondCard);
            }

            lock (partystate.OpponentCards)
            {
                partystate.OpponentCards.Add(firstCard);
                partystate.OpponentCards.Add(secondCard);
            }

            partystate.WhoIsOnPrikupNow = null;
            partystate.IsBattle = true;
            partystate.WhoIsSetBetNow = null;
            partystate.WhoIsHitNow = player.PlayerName;
           
            return partystate;
        }
        public PartyState Hit(Player player, Card card)
        {
            if (partystate.IsBattle && card.IsCorrect(partystate.PrioritySuit, partystate.DeckGamerList))
            {
                var bribe = Gamer.Bribe;
                partystate.CheckTrump(ref bribe, card, partystate.DeckGamerList);
                Gamer.Bribe = bribe;
                lock (partystate.DeckGamerList)
                {
                    partystate.DeckGamerList.Remove(card);
                    partystate.WhoIsHitNow = null;
                }
                lock (partystate.Table)
                {
                    partystate.Table.Add(player.PlayerName, card);
                }



                if (partystate.Table.Count != 2)
                    partystate.WhoIsHitNow = "Gomer";

                if (partystate.Table.Count == 1)
                {
                    partystate.PrioritySuit = partystate.Table.Values.ToList()[0].Suit;
                }
            }
           
            return partystate;
        }
        public PartyState ExitGame()
        {
            if (Gamer.Points == 1000)
            {
                MessageBox.Show("Вы Выиграли");
                partystate.EndGame = true;
            }
            else if (OpponentPlayer.Points == 1000)
            {
                MessageBox.Show("Вы Проиграли");
                partystate.EndGame = true;
            }
            return partystate;
        }
        public PartyState SetBet(Player player, int points)
        {
            if (points <= player.PotentialPoints && points > player.CurrentBet && points > AnotherPlayer(player).CurrentBet)
            {
                player.CurrentBet = points;
                partystate.OpponentCurrentBet = OpponentPlayer.CurrentBet;
                partystate.GamerCurrentBet = Gamer.CurrentBet;
                partystate.WhoIsSetBetNow = AnotherName(player);
                partystate.OpponentCards = OpponentPlayer.Cards;
                partystate.DeckGamerList = Gamer.Cards;
            }
            else if (player.PotentialPoints <= AnotherPlayer(player).CurrentBet)
                partystate = NoSetBet(player);
            return partystate;
        }
        public PartyState NoSetBet(Player player)
        {
            Player p = AnotherPlayer(player);
            lock (p.Cards)
            {
                p.Cards.AddRange(partystate.Prikup);
            }

            partystate.WhoIsOnPrikupNow = p.PlayerName;
            partystate.WhoIsSetBetNow = null;

            if (player == OpponentPlayer)
            {
                partystate.OpponentCurrentBet = 0;
                OpponentPlayer.CurrentBet = 0;
            }
            if (player == Gamer)
            {
                partystate.GamerCurrentBet = 0;
                Gamer.CurrentBet = 0;
            }
          
            partystate.WhoIsOnPrikupNow = p.PlayerName;
            partystate.WhoIsHitNow = null;
            partystate.OpponentCards = OpponentPlayer.Cards;
            partystate.DeckGamerList = Gamer.Cards;
            return partystate;
        }
        public PartyState GetPartyState()
        {
            return partystate;
        }
        public PartyState StartParty()
        {
            DefaultHundredPoints = AnotherPlayer(DefaultHundredPoints);
            if (Gamer == DefaultHundredPoints)
            {
                partystate.IsOnHundredPoints = Gamer.PlayerName;
                partystate.WhoIsSetBetNow = OpponentPlayer.PlayerName;
                partystate = SetBet(OpponentPlayer, 100);
                OpponentPlayer.partyState = partystate;
                //SendPartyState(11000);
            }
            else if (OpponentPlayer == DefaultHundredPoints)
            {
                partystate.IsOnHundredPoints = Gamer.PlayerName;
                partystate.WhoIsSetBetNow = Gamer.PlayerName;
                partystate = SetBet(Gamer, 100);
                OpponentPlayer.partyState = partystate;
                //SendPartyState(11000);
            }
            partystate.GamerCurrentBet = Gamer.CurrentBet;
            partystate.OpponentCurrentBet = OpponentPlayer.CurrentBet;
            OpponentPlayer.CalculatePotentialPoints();
            partystate.BotPotentialPoints = OpponentPlayer.PotentialPoints;
            return partystate;
        }
    }
}
