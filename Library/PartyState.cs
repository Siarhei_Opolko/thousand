﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Media;
using System.Windows.Forms;

namespace Library
{
    [Serializable]
    public class PartyState
    {
        public List<Card> DeckGamerList { get; set; }
        public List<Card> BonusListCards { get; set; }
        public List<Card> OpponentCards { get; set; }
        public Dictionary<string, Card> Table { get; set; }
        public List<Card> Prikup { get; set; }
        public bool IsBattle { get; set; }
        public bool IsLastBribe { get; set; }
        public SuitCard TrumpSuit { get; set; }
        public SuitCard PrioritySuit { get; set; }
        public int GamerCurrentBet { get; set; }
        public int OpponentCurrentBet { get; set; }
        public string WhoIsHitNow { get; set; }
        public string WhoIsSetBetNow { get; set; }
        public string WhoIsOnPrikupNow { get; set; }
        public string IsOnHundredPoints { get; set; }
        public string PlayerName { get; set; }
        public int BotBribes { get; set; }
        public bool EndGame { get; set; }
        public int BotPotentialPoints { get; set; }
        public void CheckTrump(ref int br, Card card, List<Card> pCards )
        {
            if (card.Value == ValueCard.Queen || card.Value == ValueCard.King)
            {
                if (pCards.Exists(c => (c.Value == ValueCard.King && c.Suit == card.Suit)
                && pCards.Exists(d => d.Value == ValueCard.Queen && d.Suit == card.Suit)) && br != 0 && Table.Count == 0)
                {
                    TrumpSuit = card.Suit;
                    using (SoundPlayer pl = new SoundPlayer(Path.Combine(Application.StartupPath, @"..\..\\1000MAR.wav")))
                    {
                        pl.Play();
                    }

                    if (TrumpSuit == SuitCard.Spades) br += 40;
                    else if (TrumpSuit == SuitCard.Clubs) br += 60;
                    else if (TrumpSuit == SuitCard.Diamonds) br += 80;
                    else if (TrumpSuit == SuitCard.Hearts) br += 100;
                }
            }
        }
        public void Reset()
        {
            DeckGamerList.Clear();
            OpponentCards.Clear();
            BonusListCards.Clear();
            Table.Clear();
            Prikup.Clear();
            IsLastBribe = false;
            TrumpSuit = SuitCard.None;
            PrioritySuit = SuitCard.None;
            GamerCurrentBet = 0;
            OpponentCurrentBet = 0;
            IsBattle = false;
            BotPotentialPoints = 0;
            BotBribes = 0;
        }

        public string FindBestCardOnTable(string g, string b)
        {
            if (Table[g].Suit == Table[b].Suit)
            {
                PrioritySuit = SuitCard.None;
                return (Table[g].Value > Table[b].Value) ? g : b;
            }

            if (TrumpSuit == SuitCard.None)
            {
                if (Table[g].Suit == PrioritySuit && Table[b].Suit != PrioritySuit)
                {
                    PrioritySuit = SuitCard.None;
                    return g;
                }
                PrioritySuit = SuitCard.None;
                return b;
            }
            else if (TrumpSuit != SuitCard.None)
            {
                if (Table[g].Suit != TrumpSuit && Table[b].Suit == TrumpSuit)
                {
                    PrioritySuit = SuitCard.None;
                    return b;
                }
                if (Table[g].Suit == TrumpSuit && Table[b].Suit != TrumpSuit)
                {
                    PrioritySuit = SuitCard.None;
                    return g;
                }
                if (Table[g].Suit != TrumpSuit && Table[b].Suit != TrumpSuit)
                {
                    if (Table[g].Suit == PrioritySuit)
                    {
                        PrioritySuit = SuitCard.None;
                        return g;
                    }
                    PrioritySuit = SuitCard.None;
                    return b;
                }
            }
            return null;
        }

        public PartyState()
        {
            DeckGamerList = new List<Card>();
            OpponentCards = new List<Card>();
            BonusListCards = new List<Card>();
            Table = new Dictionary<string, Card>();
            Prikup = new List<Card>();
            IsLastBribe = false;
            IsBattle = false;
            EndGame = false;
        }

        public override string ToString()
        {
            return String.Format("\nСейчас {0} на 100\nСейчас {1} на прикупе\nСейчас {2} делает ставку\nСейчас {3} делает ход\n",
                IsOnHundredPoints, WhoIsOnPrikupNow, WhoIsSetBetNow, WhoIsHitNow);
        }
    }
}
