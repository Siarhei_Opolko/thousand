﻿namespace _1000._2
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.CardFromGamer = new System.Windows.Forms.PictureBox();
            this.CardFromOpponent = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.EndMove = new System.Windows.Forms.Button();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.pictureBox28 = new System.Windows.Forms.PictureBox();
            this.SetBet = new System.Windows.Forms.Button();
            this.NoSet = new System.Windows.Forms.Button();
            this.worker = new System.ComponentModel.BackgroundWorker();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.CurrentTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.MyBribes = new System.Windows.Forms.ToolStripStatusLabel();
            this.HitCard = new System.Windows.Forms.ToolStripStatusLabel();
            this.OppBet = new System.Windows.Forms.TextBox();
            this.DesiredPoints = new System.Windows.Forms.TextBox();
            this.DownBet = new System.Windows.Forms.Button();
            this.UpBet = new System.Windows.Forms.Button();
            this.TrumpPb = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CardFromGamer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CardFromOpponent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).BeginInit();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrumpPb)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox8.ErrorImage = null;
            this.pictureBox8.Location = new System.Drawing.Point(444, 389);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(48, 76);
            this.pictureBox8.TabIndex = 24;
            this.pictureBox8.TabStop = false;
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4});
            this.listView1.Location = new System.Drawing.Point(707, 12);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(155, 306);
            this.listView1.TabIndex = 32;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Width = 73;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Width = 101;
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox15.ErrorImage = null;
            this.pictureBox15.Location = new System.Drawing.Point(91, 196);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(48, 76);
            this.pictureBox15.TabIndex = 31;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox14.ErrorImage = null;
            this.pictureBox14.Location = new System.Drawing.Point(63, 166);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(48, 76);
            this.pictureBox14.TabIndex = 30;
            this.pictureBox14.TabStop = false;
            // 
            // CardFromGamer
            // 
            this.CardFromGamer.BackColor = System.Drawing.Color.Khaki;
            this.CardFromGamer.Location = new System.Drawing.Point(383, 206);
            this.CardFromGamer.Name = "CardFromGamer";
            this.CardFromGamer.Size = new System.Drawing.Size(48, 76);
            this.CardFromGamer.TabIndex = 29;
            this.CardFromGamer.TabStop = false;
            // 
            // CardFromOpponent
            // 
            this.CardFromOpponent.BackColor = System.Drawing.Color.Khaki;
            this.CardFromOpponent.Location = new System.Drawing.Point(383, 114);
            this.CardFromOpponent.Name = "CardFromOpponent";
            this.CardFromOpponent.Size = new System.Drawing.Size(48, 76);
            this.CardFromOpponent.TabIndex = 27;
            this.CardFromOpponent.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox10.ErrorImage = null;
            this.pictureBox10.Location = new System.Drawing.Point(510, 389);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(48, 76);
            this.pictureBox10.TabIndex = 26;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox9.ErrorImage = null;
            this.pictureBox9.Location = new System.Drawing.Point(477, 389);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(48, 76);
            this.pictureBox9.TabIndex = 25;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(209, 389);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(48, 76);
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox2.ErrorImage = null;
            this.pictureBox2.Location = new System.Drawing.Point(239, 389);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(48, 76);
            this.pictureBox2.TabIndex = 18;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox3.ErrorImage = null;
            this.pictureBox3.Location = new System.Drawing.Point(270, 389);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(48, 76);
            this.pictureBox3.TabIndex = 19;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox4.ErrorImage = null;
            this.pictureBox4.Location = new System.Drawing.Point(305, 389);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(48, 76);
            this.pictureBox4.TabIndex = 20;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox5.ErrorImage = null;
            this.pictureBox5.Location = new System.Drawing.Point(338, 389);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(48, 76);
            this.pictureBox5.TabIndex = 21;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox6.ErrorImage = null;
            this.pictureBox6.Location = new System.Drawing.Point(374, 389);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(48, 76);
            this.pictureBox6.TabIndex = 22;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox7.ErrorImage = null;
            this.pictureBox7.Location = new System.Drawing.Point(407, 389);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(48, 76);
            this.pictureBox7.TabIndex = 23;
            this.pictureBox7.TabStop = false;
            // 
            // EndMove
            // 
            this.EndMove.Location = new System.Drawing.Point(466, 341);
            this.EndMove.Name = "EndMove";
            this.EndMove.Size = new System.Drawing.Size(100, 23);
            this.EndMove.TabIndex = 42;
            this.EndMove.Text = "Hit";
            this.EndMove.UseVisualStyleBackColor = true;
            this.EndMove.Click += new System.EventHandler(this.EndMove_Click);
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox11.ErrorImage = null;
            this.pictureBox11.Location = new System.Drawing.Point(546, 389);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(48, 76);
            this.pictureBox11.TabIndex = 43;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox12.ErrorImage = null;
            this.pictureBox12.Location = new System.Drawing.Point(579, 389);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(48, 76);
            this.pictureBox12.TabIndex = 44;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox13.ErrorImage = null;
            this.pictureBox13.Location = new System.Drawing.Point(37, 142);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(48, 76);
            this.pictureBox13.TabIndex = 45;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox16.ErrorImage = null;
            this.pictureBox16.Location = new System.Drawing.Point(118, 231);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(48, 76);
            this.pictureBox16.TabIndex = 46;
            this.pictureBox16.TabStop = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox17.ErrorImage = null;
            this.pictureBox17.Location = new System.Drawing.Point(579, 17);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(48, 76);
            this.pictureBox17.TabIndex = 58;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox18.ErrorImage = null;
            this.pictureBox18.Location = new System.Drawing.Point(546, 17);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(48, 76);
            this.pictureBox18.TabIndex = 57;
            this.pictureBox18.TabStop = false;
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox19.ErrorImage = null;
            this.pictureBox19.Location = new System.Drawing.Point(510, 17);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(48, 76);
            this.pictureBox19.TabIndex = 56;
            this.pictureBox19.TabStop = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox20.ErrorImage = null;
            this.pictureBox20.Location = new System.Drawing.Point(477, 17);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(48, 76);
            this.pictureBox20.TabIndex = 55;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox21.ErrorImage = null;
            this.pictureBox21.Location = new System.Drawing.Point(444, 17);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(48, 76);
            this.pictureBox21.TabIndex = 54;
            this.pictureBox21.TabStop = false;
            // 
            // pictureBox22
            // 
            this.pictureBox22.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox22.ErrorImage = null;
            this.pictureBox22.Location = new System.Drawing.Point(407, 17);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(48, 76);
            this.pictureBox22.TabIndex = 53;
            this.pictureBox22.TabStop = false;
            // 
            // pictureBox23
            // 
            this.pictureBox23.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox23.ErrorImage = null;
            this.pictureBox23.Location = new System.Drawing.Point(374, 17);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(48, 76);
            this.pictureBox23.TabIndex = 52;
            this.pictureBox23.TabStop = false;
            // 
            // pictureBox24
            // 
            this.pictureBox24.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox24.ErrorImage = null;
            this.pictureBox24.Location = new System.Drawing.Point(338, 17);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(48, 76);
            this.pictureBox24.TabIndex = 51;
            this.pictureBox24.TabStop = false;
            // 
            // pictureBox25
            // 
            this.pictureBox25.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox25.ErrorImage = null;
            this.pictureBox25.Location = new System.Drawing.Point(305, 17);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(48, 76);
            this.pictureBox25.TabIndex = 50;
            this.pictureBox25.TabStop = false;
            // 
            // pictureBox26
            // 
            this.pictureBox26.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox26.ErrorImage = null;
            this.pictureBox26.Location = new System.Drawing.Point(270, 17);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(48, 76);
            this.pictureBox26.TabIndex = 49;
            this.pictureBox26.TabStop = false;
            // 
            // pictureBox27
            // 
            this.pictureBox27.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox27.ErrorImage = null;
            this.pictureBox27.Location = new System.Drawing.Point(239, 17);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(48, 76);
            this.pictureBox27.TabIndex = 48;
            this.pictureBox27.TabStop = false;
            // 
            // pictureBox28
            // 
            this.pictureBox28.BackColor = System.Drawing.Color.Khaki;
            this.pictureBox28.ErrorImage = null;
            this.pictureBox28.Location = new System.Drawing.Point(209, 17);
            this.pictureBox28.Name = "pictureBox28";
            this.pictureBox28.Size = new System.Drawing.Size(48, 76);
            this.pictureBox28.TabIndex = 47;
            this.pictureBox28.TabStop = false;
            // 
            // SetBet
            // 
            this.SetBet.Location = new System.Drawing.Point(288, 311);
            this.SetBet.Name = "SetBet";
            this.SetBet.Size = new System.Drawing.Size(75, 23);
            this.SetBet.TabIndex = 61;
            this.SetBet.Text = "Set Bet";
            this.SetBet.UseVisualStyleBackColor = true;
            this.SetBet.Click += new System.EventHandler(this.SetBet_Click);
            // 
            // NoSet
            // 
            this.NoSet.Location = new System.Drawing.Point(288, 341);
            this.NoSet.Name = "NoSet";
            this.NoSet.Size = new System.Drawing.Size(75, 23);
            this.NoSet.TabIndex = 62;
            this.NoSet.Text = "No Set";
            this.NoSet.UseVisualStyleBackColor = true;
            this.NoSet.Click += new System.EventHandler(this.NoSet_Click);
            // 
            // worker
            // 
            this.worker.WorkerReportsProgress = true;
            this.worker.WorkerSupportsCancellation = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CurrentTime,
            this.MyBribes,
            this.HitCard});
            this.statusStrip1.Location = new System.Drawing.Point(0, 477);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(887, 22);
            this.statusStrip1.TabIndex = 63;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // CurrentTime
            // 
            this.CurrentTime.ActiveLinkColor = System.Drawing.Color.White;
            this.CurrentTime.BackColor = System.Drawing.Color.Transparent;
            this.CurrentTime.Name = "CurrentTime";
            this.CurrentTime.Size = new System.Drawing.Size(0, 17);
            // 
            // MyBribes
            // 
            this.MyBribes.ActiveLinkColor = System.Drawing.Color.Transparent;
            this.MyBribes.BackColor = System.Drawing.Color.Transparent;
            this.MyBribes.Margin = new System.Windows.Forms.Padding(30, 3, 0, 2);
            this.MyBribes.Name = "MyBribes";
            this.MyBribes.Size = new System.Drawing.Size(0, 17);
            // 
            // HitCard
            // 
            this.HitCard.ActiveLinkColor = System.Drawing.Color.Transparent;
            this.HitCard.BackColor = System.Drawing.Color.Transparent;
            this.HitCard.Margin = new System.Windows.Forms.Padding(120, 3, 0, 2);
            this.HitCard.Name = "HitCard";
            this.HitCard.Size = new System.Drawing.Size(0, 17);
            // 
            // OppBet
            // 
            this.OppBet.Location = new System.Drawing.Point(288, 114);
            this.OppBet.Name = "OppBet";
            this.OppBet.Size = new System.Drawing.Size(40, 20);
            this.OppBet.TabIndex = 64;
            // 
            // DesiredPoints
            // 
            this.DesiredPoints.Location = new System.Drawing.Point(393, 330);
            this.DesiredPoints.Name = "DesiredPoints";
            this.DesiredPoints.Size = new System.Drawing.Size(40, 20);
            this.DesiredPoints.TabIndex = 65;
            // 
            // DownBet
            // 
            this.DownBet.Location = new System.Drawing.Point(369, 330);
            this.DownBet.Name = "DownBet";
            this.DownBet.Size = new System.Drawing.Size(27, 20);
            this.DownBet.TabIndex = 66;
            this.DownBet.Text = "-";
            this.DownBet.UseVisualStyleBackColor = true;
            this.DownBet.Click += new System.EventHandler(this.DownBet_Click);
            // 
            // UpBet
            // 
            this.UpBet.Location = new System.Drawing.Point(430, 330);
            this.UpBet.Name = "UpBet";
            this.UpBet.Size = new System.Drawing.Size(27, 20);
            this.UpBet.TabIndex = 67;
            this.UpBet.Text = "+";
            this.UpBet.UseVisualStyleBackColor = true;
            this.UpBet.Click += new System.EventHandler(this.UpBet_Click);
            // 
            // TrumpPb
            // 
            this.TrumpPb.Location = new System.Drawing.Point(707, 330);
            this.TrumpPb.Name = "TrumpPb";
            this.TrumpPb.Size = new System.Drawing.Size(155, 135);
            this.TrumpPb.TabIndex = 68;
            this.TrumpPb.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(466, 311);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 23);
            this.button1.TabIndex = 72;
            this.button1.Text = "I\'m ready";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Khaki;
            this.ClientSize = new System.Drawing.Size(887, 499);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.TrumpPb);
            this.Controls.Add(this.UpBet);
            this.Controls.Add(this.DownBet);
            this.Controls.Add(this.DesiredPoints);
            this.Controls.Add(this.OppBet);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.NoSet);
            this.Controls.Add(this.SetBet);
            this.Controls.Add(this.pictureBox17);
            this.Controls.Add(this.pictureBox18);
            this.Controls.Add(this.pictureBox19);
            this.Controls.Add(this.pictureBox20);
            this.Controls.Add(this.pictureBox21);
            this.Controls.Add(this.pictureBox22);
            this.Controls.Add(this.pictureBox23);
            this.Controls.Add(this.pictureBox24);
            this.Controls.Add(this.pictureBox25);
            this.Controls.Add(this.pictureBox26);
            this.Controls.Add(this.pictureBox27);
            this.Controls.Add(this.pictureBox28);
            this.Controls.Add(this.pictureBox16);
            this.Controls.Add(this.pictureBox15);
            this.Controls.Add(this.pictureBox14);
            this.Controls.Add(this.pictureBox13);
            this.Controls.Add(this.pictureBox12);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.EndMove);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.CardFromGamer);
            this.Controls.Add(this.CardFromOpponent);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "1000";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CardFromGamer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CardFromOpponent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrumpPb)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.PictureBox pictureBox8;
        public System.Windows.Forms.ListView listView1;
        public System.Windows.Forms.PictureBox pictureBox15;
        public System.Windows.Forms.PictureBox pictureBox14;
        public System.Windows.Forms.PictureBox CardFromGamer;
        public System.Windows.Forms.PictureBox CardFromOpponent;
        public System.Windows.Forms.PictureBox pictureBox10;
        public System.Windows.Forms.PictureBox pictureBox9;
        public System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.PictureBox pictureBox2;
        public System.Windows.Forms.PictureBox pictureBox3;
        public System.Windows.Forms.PictureBox pictureBox4;
        public System.Windows.Forms.PictureBox pictureBox5;
        public System.Windows.Forms.PictureBox pictureBox6;
        public System.Windows.Forms.PictureBox pictureBox7;
        public System.Windows.Forms.PictureBox pictureBox17;
        public System.Windows.Forms.PictureBox pictureBox18;
        public System.Windows.Forms.PictureBox pictureBox19;
        public System.Windows.Forms.PictureBox pictureBox20;
        public System.Windows.Forms.PictureBox pictureBox21;
        public System.Windows.Forms.PictureBox pictureBox22;
        public System.Windows.Forms.PictureBox pictureBox23;
        public System.Windows.Forms.PictureBox pictureBox24;
        public System.Windows.Forms.PictureBox pictureBox25;
        public System.Windows.Forms.PictureBox pictureBox26;
        public System.Windows.Forms.PictureBox pictureBox27;
        public System.Windows.Forms.PictureBox pictureBox28;
        public System.Windows.Forms.Button EndMove;
        public System.Windows.Forms.PictureBox pictureBox11;
        public System.Windows.Forms.PictureBox pictureBox12;
        public System.Windows.Forms.PictureBox pictureBox13;
        public System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.Button SetBet;
        private System.Windows.Forms.Button NoSet;
        public System.ComponentModel.BackgroundWorker worker;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel CurrentTime;
        public System.Windows.Forms.TextBox OppBet;
        private System.Windows.Forms.TextBox DesiredPoints;
        private System.Windows.Forms.Button DownBet;
        private System.Windows.Forms.Button UpBet;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ToolStripStatusLabel MyBribes;
        private System.Windows.Forms.ToolStripStatusLabel HitCard;
        private System.Windows.Forms.PictureBox TrumpPb;
        private System.Windows.Forms.Button button1;

    }
}

