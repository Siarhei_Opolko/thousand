﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library;

namespace _1000._2
{
    public partial class Form1 : Form
    {
        public List<PictureBox> DeckList;
        public List<PictureBox> TableList;
        public List<PictureBox> OpponentList;
        public List<PictureBox> BonusCardsPictureBoxs;
        public PartyState partystate = new PartyState();
        private Game game;
        
        private delegate void SetTextHandler(TextBox c, string str);

        private delegate void AddItemHandler(ListView l, ListViewItem item);
       
        public Form1(string name)
        {
            InitializeComponent();

            TableList = new List<PictureBox> { CardFromGamer, CardFromOpponent };
            DeckList = new List<PictureBox> { pictureBox1, pictureBox2, pictureBox3, pictureBox4, pictureBox5, pictureBox6, pictureBox7, pictureBox8, pictureBox9, pictureBox10, pictureBox11, pictureBox12 };

            OpponentList = new List<PictureBox> { pictureBox17, pictureBox18, pictureBox19, pictureBox20, pictureBox21, pictureBox22, pictureBox23, pictureBox24, pictureBox25, pictureBox26, pictureBox27, pictureBox28 };

            BonusCardsPictureBoxs = new List<PictureBox> { pictureBox13, pictureBox14, pictureBox15, pictureBox16 };
            game = new Game();
            game.Gamer.PlayerName = name;
            partystate.PlayerName = name;
            game.OpponentPlayer.PlayerName = "Gomer";
            listView1.Columns[0].Text = game.Gamer.PlayerName;
            listView1.Columns[1].Text = game.OpponentPlayer.PlayerName;
            listView1.Items.Add(new ListViewItem(new string[] { "0", "0" }));
           
            game.DefaultHundredPoints = game.Gamer;
            Components.InitCards();
            Components.InitSuits();
            InitEvents();
            CheckTable();
            
            RunShowStatusStrip();
            partystate.Reset();
            partystate = game.InitCardsForParty();
            SendPartyState(11000);
            Redraw(partystate);
        }

        public void SendPartyState(int port)
        {
            byte[] bytes = new byte[8192];

            // Устанавливаем удаленную точку для сокета
            IPHostEntry ipHost = Dns.GetHostEntry("localhost");
            IPAddress ipAddr = ipHost.AddressList[0];
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, port);

            Socket sender = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            // Соединяем сокет с удаленной точкой
            sender.Connect(ipEndPoint);

            Console.WriteLine("Сокет соединяется с {0} ", sender.RemoteEndPoint.ToString());//
            byte[] msg = Tools.Serialize(partystate);

            // Отправляем данные через сокет
            int bytesSent = sender.Send(msg);

            // Получаем ответ от сервера
            int bytesRec = sender.Receive(bytes);

            partystate = Tools.Deserialize(bytes);
            game.partystate = partystate;
            Redraw(partystate);
           
            // Освобождаем сокет
            sender.Shutdown(SocketShutdown.Both);
            sender.Close();
        }

        private void RunShowStatusStrip()
        {
            worker.DoWork += (sender, args) =>
            {
                while (true)
                {
                    Thread.Sleep(1000);
                    CurrentTime.Text = "Current time " + DateTime.Now.ToLongTimeString();
                    MyBribes.Text = "My bribes: " + game.Gamer.Bribe.ToString();
                    HitCard.Text = "Hit Card: " + game.Gamer.OutputCard.ToString();
                    TrumpPb.Image = partystate.TrumpSuit != SuitCard.None ? Components.SuitsBitmaps[partystate.TrumpSuit].Clone() as Bitmap : null;
                }
            };
            worker.RunWorkerAsync();
        }

        private static int CompareCards(Card x, Card y)
        {
            if (x.GetHashCode() > y.GetHashCode()) return 1;
            return -1;
        }

        public void Redraw(PartyState partyState)
        {
            int i = 0;
            for (i = 0; i < 12; i++)
                DeckList[i].Image = null;

            partyState.DeckGamerList.Sort(CompareCards);

            //Отрисовка карт игроков с конца
            //var maxIndex = partyState.DeckGamerList.Count - 1;
            //var picture = 11 - maxIndex;
            //for (int i1 = 11; i1 >= picture; i1--)
            //{
            //    DeckList[i1].Image = game.Gamer.Cards[maxIndex].GetBitmap();
            //    DeckList[i1].Tag = game.Gamer.Cards[maxIndex];
            //    --maxIndex;
            //}

            i = FindIndex(partyState.DeckGamerList.Count);
            foreach (var c in partyState.DeckGamerList)
            {
                DeckList[i].Image = c.GetBitmap();
                DeckList[i].Tag = c;
                i++;
            }

            TableList.ForEach(p => p.Image = null);
            if (partyState.Table.ContainsKey("Gomer"))
                CardFromOpponent.Image = partyState.Table["Gomer"].GetBitmap();

            if (partyState.Table.ContainsKey(game.Gamer.PlayerName))
                CardFromGamer.Image = partyState.Table[game.Gamer.PlayerName].GetBitmap();

            for (i = 0; i < 4; i++)
                BonusCardsPictureBoxs[i].Image = partyState.IsLastBribe ? partyState.BonusListCards[i].GetBitmap() : Card.GetFlipside();

            partyState.OpponentCards.Sort(CompareCards);

            OpponentList.ForEach(pic => pic.Image = null);
            i = FindIndex(partyState.OpponentCards.Count);
            foreach (var c in partyState.OpponentCards)
                OpponentList[i++].Image = Card.GetFlipside();
            
            if (!partyState.IsBattle)
            {
                SetText(OppBet, partystate.OpponentCurrentBet.ToString());
                SetText(DesiredPoints, partystate.GamerCurrentBet.ToString());
            }

            if (partyState.EndGame)
            {
                Close();
            }
        }

        private int FindIndex(int count)
        {
            int index = (12 - count) / 2;
            if (index != 0) ++index;
            return index;
        }

        private void AddItem(ListView l, ListViewItem item)
        {
            if (l.InvokeRequired)
            {
                AddItemHandler a = new AddItemHandler(AddItem);
                Invoke(a, new object[] { l, item });
            }
            else
            {
                l.Items.Add(item);
            }
        }

        private void SetText(TextBox c, string text)
        {
            if (c.InvokeRequired)
            {
                SetTextHandler d = new SetTextHandler(SetText);
                Invoke(d, new object[] { c, text });
            }
            else
            {
                c.Text = text;
            }
        }

        private void InitBotEvents()
        {
            Task.Run(() =>
            {
                while (true)
                {
                    Thread.Sleep(100);
                    if (game.OpponentPlayer.PlayerName == game.OpponentPlayer.partyState.WhoIsSetBetNow)
                    {
                        partystate = game.SetBet(game.OpponentPlayer, game.OpponentPlayer.SetDesiredPoints(game.Gamer.CurrentBet));
                        Redraw(partystate);
                    }
                    else if (game.OpponentPlayer.PlayerName == game.OpponentPlayer.partyState.WhoIsOnPrikupNow)
                    {
                        Card a;
                        Card b;
                        game.OpponentPlayer.SelectTwoCards(out a, out b);
                        partystate = game.GiveAwayCards(game.OpponentPlayer, a, b);
                        Redraw(partystate);
                    }
                    else if (game.OpponentPlayer.PlayerName == game.OpponentPlayer.partyState.WhoIsHitNow && partystate.IsBattle)
                    {
                        if (game.OpponentPlayer.Cards.Count != 0)
                        {
                            partystate = game.Hit(game.OpponentPlayer, game.OpponentPlayer.GiveCard());
                            Redraw(partystate);
                        }
                    }
                }
            });
        }
        private void CheckTable()
        {
            Player p;string pName = null;
            Task.Run(() =>
            {
                while (true)
                {
                    Thread.Sleep(1000);
                    if (partystate.Table.Count == 2)
                    {
                        pName = partystate.FindBestCardOnTable( game.Gamer.PlayerName, "Gomer");
                        p = game.GetPlayerByName(pName);
                        p.Bribe += partystate.Table.Values.ToList().Sum(t => t.GetPointsCard());
                        if (partystate.DeckGamerList.Count == 0 && partystate.OpponentCards.Count == 0)
                        {
                            p.Bribe += partystate.BonusListCards.Sum(x => x.GetPointsCard());
                        }
                        partystate.WhoIsHitNow = p.PlayerName;
                        game.Gamer.OutputCard.NoCard();
                        game.OpponentPlayer.OutputCard.NoCard();
                        partystate.Table.Clear();
                        if (partystate.DeckGamerList.Count == 1 && partystate.OpponentCards.Count == 1)
                            partystate.IsLastBribe = true;
                        partystate.PrioritySuit = SuitCard.None;
                        
                        SendPartyState(11000); Redraw(partystate);
                    }

                    if (partystate.DeckGamerList.Count == 0 && partystate.OpponentCards.Count == 0)
                    {
                        AnalyzeResultsParty();
                        partystate.Reset();
                        partystate = game.InitCardsForParty();
                        SendPartyState(11000);
                        Redraw(partystate);
                    }
                }
            });
        }

        private void AnalyzeResultsParty()
        {
            game.partystate = partystate;
            game.Gamer.CurrentBet = partystate.GamerCurrentBet;
            game.Gamer.IsPlayedBets();
            game.OpponentPlayer.Bribe += partystate.BotBribes;
            game.OpponentPlayer.CurrentBet = partystate.OpponentCurrentBet;
            game.OpponentPlayer.IsPlayedBets();
            game.Gamer.Is555Points();
            game.OpponentPlayer.Is555Points();
            string g = null, o = null;
            g = GetPointsString(game.Gamer);
            
            o = GetPointsString(game.OpponentPlayer);

            AddItem(listView1, new ListViewItem(new string[] { g, o }));

            if (game.Gamer.Points != 1000 && game.OpponentPlayer.Points != 1000)
            {
                partystate.Reset();
                game.InitCardsForParty();
                Redraw(partystate);
            }
        }
        private string GetPointsString(Player p)
        {
            if (p.IsNoBribe())
            {
                p.Bolts++;
                if (p.Bolts == 3)
                {
                    p.Bolts = 0;
                    p.Points -= 120;
                    return p.Points.ToString();
                }
                return "-";
            }
            return p.Points.ToString();
        }
        private void InitEvents()
        {
            for (int i = 0; i < 12; i++)
            {
                DeckList[i].Click += (sender, e) =>
                {
                    if (game.Gamer.PlayerName == partystate.WhoIsHitNow && partystate.IsBattle)
                    {
                        try
                        {
                            var c = (Card)(sender as PictureBox).Tag;
                            game.Gamer.OutputCard = c;
                        }
                        catch (NullReferenceException ex)
                        {
                            MessageBox.Show("It's no card");
                        }
                    }
                    else if (game.Gamer.PlayerName == partystate.WhoIsOnPrikupNow)
                    {
                        try
                        {
                            var c = (Card)(sender as PictureBox).Tag;
                            MessageBox.Show("я отдал " + c.ToString());
                            game.Gamer.OutputCards.Add(c);

                            if (game.Gamer.OutputCards.Count == 2)
                            {
                                partystate = game.GiveAwayCards(game.Gamer, game.Gamer.OutputCards[0],
                                    game.Gamer.OutputCards[1]);
                                Redraw(partystate);
                            }
                        }
                        catch (NullReferenceException ex)
                        {
                            MessageBox.Show("It's no card");
                        }
                    }
                };
            }
        }
        private bool IsNoCard()
        {
            if (game.Gamer.OutputCard.Suit == SuitCard.None && game.Gamer.OutputCard.Value == ValueCard.None)
                return true;
            return false;
        }

        private void EndMove_Click(object sender, EventArgs e)
        {
            if (game.Gamer.PlayerName == partystate.WhoIsHitNow && !IsNoCard())
            {
                partystate = game.Hit(game.Gamer, game.Gamer.OutputCard);
                Redraw(partystate);
                game.OpponentPlayer.partyState = partystate;
                SendPartyState(11000);
            }
        }

        private void SetBet_Click(object sender, EventArgs e)
        {
            if (game.Gamer.PlayerName == partystate.WhoIsSetBetNow)
            {
                  partystate = game.SetBet(game.Gamer, int.Parse(DesiredPoints.Text));
                  Redraw(partystate);
                  game.OpponentPlayer.partyState = partystate;
                  SendPartyState(11000);
            }
        }

        private void NoSet_Click(object sender, EventArgs e)
        {
            if (game.Gamer.PlayerName == partystate.WhoIsSetBetNow)
            {
                game.NoSetBet(game.Gamer);
                Redraw(partystate);
                game.OpponentPlayer.partyState = partystate;
                SendPartyState(11000);
            }
        }

        private void DownBet_Click(object sender, EventArgs e)
        {
            if (game.Gamer.PlayerName == partystate.WhoIsSetBetNow)
                DesiredPoints.Text = (int.Parse(DesiredPoints.Text) - 5).ToString();
        }

        private void UpBet_Click(object sender, EventArgs e)
        {
            if (game.Gamer.PlayerName == partystate.WhoIsSetBetNow)
                DesiredPoints.Text = (int.Parse(DesiredPoints.Text) + 5).ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(partystate.WhoIsHitNow == "Gomer")
            SendPartyState(11000);
        }
    }
}
