﻿using System;
using System.Windows.Forms;

namespace _1000._2
{
    public partial class StartForm : Form
    {
        public StartForm()
        {
            InitializeComponent();
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            if(textBoxPlayerName.Text != null && textBoxPlayerName.Text != "Gomer")
            {
                var f = new Form1(textBoxPlayerName.Text);
                f.Show();
            }
        }
    }
}
