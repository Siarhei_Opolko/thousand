﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Library;

namespace Server
{
    public class Server  
    {
        public static Library.Bot B= new Library.Bot();
        private static PartyState ps = new PartyState();
        public static Random random = new Random();

        private static void DoSomething()
        {
            if (ps.WhoIsSetBetNow == "Gomer")
            {
                B.partyState = ps;
                if (random.NextDouble() < 0.45)
                {
                    ps = B.SetBet(null, ps.GamerCurrentBet + 5);
                    Console.WriteLine("я сделал ставку на {0} очков и передал ход {1}",ps.OpponentCurrentBet,ps.PlayerName);
                }
                else
                {
                    ps = B.NoSetBet(null);
                    Console.WriteLine("я пасс в игре с {0}",ps.PlayerName);
                }
               
            }
            else if (ps.WhoIsOnPrikupNow == "Gomer")
            {
                B.partyState = ps;
                Card a;Card b;
                B.SelectTwoCards(out a, out b);
                ps = B.GiveAwayCards(null, a, b);
                Console.WriteLine("я отдал 2 карты {0} и {1} игроку {2}\n",a.ToString(),b.ToString(),ps.PlayerName);
            }
            else if (ps.WhoIsHitNow == "Gomer")
            {
                B.partyState = ps;
                if (ps.OpponentCards.Count != 0)
                {   
                    Card c = B.GiveCard();
                    ps = B.Hit(null, c);
                    Console.WriteLine("я походил картой {0} на стол с игроком {1}", c.ToString(), ps.PlayerName);
                }
            }
        }
        private static void Main(string[] args)
        {
           //Thread thread = new Thread(ServerMethod);
           //thread.Start();
            ServerMethod();
        }

        private static void ServerMethod()
        {
            // Устанавливаем для сокета локальную конечную точку
            IPHostEntry ipHost = Dns.GetHostEntry("localhost");
            IPAddress ipAddr = ipHost.AddressList[0];
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, 11000);

            // Создаем сокет Tcp/Ip
            Socket sListener = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            // Назначаем сокет локальной конечной точке и слушаем входящие сокеты
            try
            {
                sListener.Bind(ipEndPoint);
                sListener.Listen(10);

                // Начинаем слушать соединения
                while (true)
                {
                    Console.WriteLine("Ожидаю входящих подключений");

                    // Программа приостанавливается, ожидая входящее соединение
                    Socket handler = sListener.Accept();

                    var workingThread = new Thread(new ParameterizedThreadStart(ProcessSocketConnection));
                    // Мы дождались клиента, пытающегося с нами соединиться
                    workingThread.Start(handler);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                Console.ReadLine();
            }
        }

        private static void ProcessSocketConnection(Object obj)
        {
            var handler = obj as Socket;
            byte[] bytes = new byte[8192];
            int bytesRec = handler.Receive(bytes);

            ps = Tools.Deserialize(bytes);

            Console.Write("Принят partystate от игрока {0}\n", ps.PlayerName);

            DoSomething();

            byte[] msg = Tools.Serialize(ps);
            handler.Send(msg);
            Console.WriteLine("Я переслал partystate игроку {0}\n",ps.PlayerName);

            handler.Shutdown(SocketShutdown.Both);
            handler.Close();
        }
    }
}
